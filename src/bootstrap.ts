import { Engine, Position2D, logger, LogLevel, Rotation2D } from '../tingine/lib'
import { System } from '../tingine/lib/Core/System'
import GraphicsEngine from '../tingine/lib/Extras/Graphics/GraphicsEngine'
import GraphicsViewComponent from '../tingine/lib/Extras/Graphics/GraphicsViewComponent'
import Input from '../tingine/lib/Extras/Input/Input'
import StatusBar from '../tingine/lib/Extras/StatusBar/StatusBar'
import { AlignmentType } from '../tingine/lib/Extras/Graphics/AlignmentType'
import { PointerComponent } from '../tingine/lib/Extras/Input/PointerComponent'
import { TextData, SpriteByNameData } from '../tingine/lib/Extras/Graphics/components'
import { Rect } from '../tingine/lib/Extras/Graphics/Rect'
import V2D from '../tingine/lib/Extras/Maths/V2D'

import styles from '../resources/styles.sass'
import { topRaceSS } from '../resources'

export const engine = new Engine()

logger.SetLevel(LogLevel.Trace)

export const graphics = new GraphicsEngine(engine)
graphics.canvas.className = styles.canvas
graphics.clearColor = '#222222'

export const statusBar = new StatusBar(engine)
statusBar.dom.className = styles.status

export const input = new Input(engine, graphics.canvas)

export const skin = {
  buttonBgColor: '#A3703A',
  buttonFont: 'DroidSansMono',
  buttonFontSize: 14,
  buttonLabelColor: 'white',
  buttonSprite: 'panel_woodDetail.png',
  labelFontColor: '#A3703A',
  labelFontDecor: '',
  labelFontFace: 'DroidSansMono',
  labelFontSize: 14,
  panelBgColor: '#FFF1D2DD',
  panelSprite: 'panel_woodDetail_blank.png',
}

export type func = () => void

export function createLable(
  pos: Position2D,
  label: string,
  fontSize: number = 16,
  color: string = 'white',
  fontDecorations: string = '',
  onUp: func[] = [],
  rect?: Rect,
) {
  return engine.addEntity([
    pos,
    new GraphicsViewComponent(new TextData({
      alignment: AlignmentType.C,
      color,
      fontDecorations,
      fontFace: 'verdana',
      fontSize,
      label,
    })),
    new PointerComponent(
      [],
      [],
      onUp,
      rect
    )
  ])
}

export const anchor = graphics.anchors.CT

// tslint:disable: max-classes-per-file
class Car { }
class CharacterController extends Car { }
class Velocity {
  constructor(
    public vector: V2D
  ) { }
}

export const clear = () => {
  engine.removeAllEntities()
}

export const menuScene = () => {
  clear()

  createLable(new Position2D(0, 80, anchor), 'Top Race', 72, 'white', 'bold')

  createLable(new Position2D(0, 165, anchor), 'Play', 25, 'white', 'bold'
    , [clear, gameScene], new Rect(-50, -10, 100, 20))

  createLable(new Position2D(0, 260, anchor), `
Instructions!
  `.trim(), 16, 'white', '')
}

export const gameScene = () => {
  clear()

  createLable(new Position2D(0, 80, anchor), 'Score: ' + 0, 32, 'white', 'bold')

  createLable(new Position2D(0, 165, anchor), 'Back to Menu', 25, 'white', 'bold'
    , [clear, menuScene], new Rect(-50, -10, 100, 20))

  const len = 5
  const sprites: { [i in number]: string } = {
    0: 'road_asphalt77.png',
    [len - 2]: 'road_asphalt43.png',
    [len - 1]: 'road_asphalt61.png',
  }
  for (let i = 0; i < len; i++) {
    engine.addEntity([
      new Position2D(100 + 128 + i * 128, 300, engine.graphics.anchors.LT),
      new GraphicsViewComponent(new SpriteByNameData({
        spriteSheet: topRaceSS,
        name: sprites[i] ?? 'road_asphalt02.png'
      }))
    ])
  }

  engine.addEntity([
    new Position2D(260, 300, engine.graphics.anchors.LT),
    new Velocity(new V2D(0, 0)),
    new GraphicsViewComponent(new SpriteByNameData({
      spriteSheet: topRaceSS,
      name: 'car_black_1.png'
    })),
    new Rotation2D(Math.PI / 2),
    new CharacterController()
  ])
}

class CharacterControllerSystem extends System {
  components = [Velocity, CharacterController]
  private v = 5

  update(_dt: number, _ts: number, _entity: number, v: Velocity): void {
    if (!input.component.leftHold) {
      if (v.vector.x > 0) {
        v.vector.apply(new V2D(-this.v / 2))
      }
      return
    }

    v.vector.apply(new V2D(this.v, 0))
  }
}

engine.addUpdateSystem(CharacterControllerSystem)

class VelocitySystem extends System {
  components = [Position2D, Velocity]
  update(dt: number, _ts: number, _entity: number, pos: Position2D, v: Velocity): void {
    if (v.vector.x > 0) {
      pos.point.apply(v.vector.mul(dt / 1000))
    }
  }
}

engine.addUpdateSystem(VelocitySystem)
