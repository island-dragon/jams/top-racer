import { SpriteSheet } from '../tingine/lib/Extras/Graphics/SpriteSheet'

import top_race_img from './top-race.png'
import top_race_data from './top-race.json'

export const topRaceSS = new SpriteSheet(top_race_img, top_race_data)
